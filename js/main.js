// 1. General SCO & completion
// ***************************
let unloaded = false;

function unloadHandler() {
  if (!unloaded) {
    doQuit();
  }
}

const exitBtn = document.querySelector('#exitBtn');
exitBtn.addEventListener('click', closeWindow);

function closeWindow() {
  doQuit();
  window.top.close();
}

loadPage();

window.onbeforeunload = unloadHandler;
window.onunload = unloadHandler;


// 2. Certificate data display (name, date)
// ****************************************
// Get data from LMS
// suspend data template: 11/2/2021|Perry Schwartz
var userName = null;
var completionDate = null;

// Get suspend
var suspend = doLMSGetValue("cmi.suspend_data");
var suspendHasData = suspend.length > 1;

// Use or generate suspend
if (suspendHasData) {
  // Read values from existing suspend data
  completionDate = suspend.substring(0, suspend.indexOf("|"));
  userName = suspend.substring(suspend.indexOf("|") + 1);
} else {
  // No suspend data, create new values
  completionDate = getTodaysDate();
  userName = doLMSGetValue("cmi.core.student_name");
  userName = fixName(userName);

  // Set in suspend
  suspend = `${completionDate}|${userName}`;
  doLMSSetValue("cmi.suspend_data", suspend);
  doLMSCommit();
}

// Complete
setScoCompleted();

// HTML elements
var userNameEl = document.getElementById("userNameShow");
var dateEl = document.getElementById("date");
var printBtn = document.getElementById("print_btn");
var contentEl = document.getElementById("content");

// Set data into elements
userNameEl.innerHTML = userName;
dateEl.innerHTML = completionDate;

printBtn.addEventListener("click", function (e) {
  window.print();
});

// Helpers
function setScoCompleted() {
  doLMSSetValue("cmi.core.lesson_status", "completed");
}

function fixName(str) {
  // Needed when name comes from LMS, which returns lastname, firstname: "Schwartz, Perry"
  if (str.indexOf(", ") != -1) {
    var arr = userName.split(", ");
    var str = arr[1] + " " + arr[0];
  }
  return str;
}

function getTodaysDate() {
  // Get today's date (format: 01/02/2018)
  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    //mm = '0' + mm;
  }

  // Numbers only
  var todayStr = mm + "/" + dd + "/" + yyyy;

  // Month string
  //var todayStr = months[today.getMonth()]+' '+dd+', '+yyyy;

  return todayStr;
}